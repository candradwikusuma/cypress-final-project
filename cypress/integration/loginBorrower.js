import Registration from '../pom/registration'
const { Random } = require('random-js');
let registerData
describe('Borrower Registration', function () {
    before(function () {
        registerData={
            "fullname": `Fullname ${randomAlphabet()}`,
            "email": `test${randomNumber()}@gmail.com`,
            "username": `username${randomNumber()}`,
            "password": "Elmacho1",
        }
        cy.visit('https://investree.tech/login', {
            auth: {
                username: 'investree',
                password: 'investme!',
            },
        });
        Registration.getRegistrationButton().click();
        Registration.getRegistrationIndividual().click();
        Registration.getRegistrationSubmitBeneficalOwner().click();

        Registration.getSalutation().click()
        Registration.getSalutationOption().click()
        Registration.getFullName().type(registerData.fullname)
        Registration.getEmail().type(registerData.email)
        Registration.getHp().type(randomNumber(12))
        Registration.getNamaPengguna().type(registerData.username)
        Registration.getPassword().type(registerData.password)
        Registration.getAgreePrivacy().check()
        Registration.getSubscribe().check()
        Registration.getSubmitButtonRegistration().click()
       


         cy.url({timeout:10000}).should('contain', 'https://investree.tech/v3/auth/otp');

         Registration.getOTP1().type(1)
         Registration.getOTP2().type(2)
         Registration.getOTP3().type(3)
         Registration.getOTP4().type(4)
         Registration.getOTP5().type(5)
         Registration.getOTP6().type(6)

         cy.url({timeout:10000}).should('contain', 'https://investree.tech/v3/auth/preferences');
         
         Registration.getBorrowerIntitusi().click()
         Registration.getEditCompany().click()
         Registration.getSelectedCompany().click()
         Registration.getCompanyName().type(`perusahaan ${randomAlphabet()}`)
         Registration.getSemuaProduk().click()
         Registration.getProjectFinancing().click()
         Registration.getSubmitContinue().click()
         
         cy.url({timeout:10000}).should('contain', 'https://investree.tech/v3/borrower/data-completion');
         cy.visit('https://investree.tech/v3/auth/logout',{
             auth:{
                 username:'investree',
                 password:'investme!'
                }
            });
        })
        beforeEach(function(){
            cy.visit("https://investree.tech/login",{
                auth:{
                    username: 'investree',
                    password: 'investme!',
                },
            })
            
        Registration.getButtonSubmit().should('be.visible') 
    })
 it('Login With Valid Credendtial Should Success',function(){
        Registration.getEmailLogin().type(registerData.email) 
        Registration.getPasswordLogin().type(registerData.password) 
        Registration.getButtonSubmit().click()
        cy.url({timeout:10000}).should('contain', 'https://investree.tech/v3/borrower/data-completion');
    })
    it('Login With Invalid Password Should Fail',function(){
        Registration.getEmailLogin().type(registerData.email) 
        Registration.getPasswordLogin().type(`${registerData.password}wrongPassword`)
        Registration.getButtonSubmit().click()
        Registration.getErrorMessage().should('contain','username dan password tidak tepat')
       
    })

})

function randomNumber(length = 9) {
    const pool = '1234567890';

    let result = new Random().string(length, pool);
    if (result.charAt(0) === '0') {
        const randomNum = new Random().integer(1, 9);
        result = randomNum + result.slice(1, result.length);
    }

    return result;
}

function randomAlphabet(length = 12) {
    const pool = 'abcdefghijklmnopqrstuvwxyz';

    let result = new Random().string(length, pool);
    if (result.charAt(0) === '0') {
        const randomAlpha = new Random().string(1, 25);
        result = randomAlpha + result.slice(1, result.length);
    }

    return result;
}