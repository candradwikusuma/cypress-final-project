import Registration from '../pom/registration'
import Shareholder from '../pom/shareholder'
const { Random } = require('random-js');
let registerData
describe('Create Shareholder information', function () {
    before(function () {
        registerData={
            "fullname": `Fullname ${randomAlphabet()}`,
            "email": `test${randomNumber()}@gmail.com`,
            "username": `username${randomNumber()}`,
            "password": "Elmacho1",
        }
        cy.visit('https://investree.tech/login', {
            auth: {
                username: 'investree',
                password: 'investme!',
            },
        });
        Registration.getRegistrationButton().click();
        Registration.getRegistrationIndividual().click();
        Registration.getRegistrationSubmitBeneficalOwner().click();

        Registration.getSalutation().click()
        Registration.getSalutationOption().click()
        Registration.getFullName().type(registerData.fullname)
        Registration.getEmail().type(registerData.email)
        Registration.getHp().type(randomNumber(12))
        Registration.getNamaPengguna().type(registerData.username)
        Registration.getPassword().type(registerData.password)
        Registration.getAgreePrivacy().check()
        Registration.getSubscribe().check()
        Registration.getSubmitButtonRegistration().click()
       


         cy.url({timeout:10000}).should('contain', 'https://investree.tech/v3/auth/otp');

         Registration.getOTP1().type(1)
         Registration.getOTP2().type(2)
         Registration.getOTP3().type(3)
         Registration.getOTP4().type(4)
         Registration.getOTP5().type(5)
         Registration.getOTP6().type(6)

         cy.url({timeout:10000}).should('contain', 'https://investree.tech/v3/auth/preferences');
         
         Registration.getBorrowerIntitusi().click()
         Registration.getEditCompany().click()
         Registration.getSelectedCompany().click()
         Registration.getCompanyName().type(`perusahaan ${randomAlphabet()}`)
         Registration.getSemuaProduk().click()
         Registration.getProjectFinancing().click()
         Registration.getSubmitContinue().click()
         
         cy.url({timeout:10000}).should('contain', 'https://investree.tech/v3/borrower/data-completion');
         cy.visit('https://investree.tech/v3/auth/logout',{
             auth:{
                 username:'investree',
                 password:'investme!'
                }
            });
        })
        beforeEach(function(){
            cy.visit("https://investree.tech/login",{
                auth:{
                    username: 'investree',
                    password: 'investme!',
                },
            })
            Registration.getEmailLogin().type(registerData.email) 
            Registration.getPasswordLogin().type(registerData.password) 
            // Registration.getEmailLogin().type('candradwikusuma11@gmail.com') 
            // Registration.getPasswordLogin().type('Elmacho99') 
            Registration.getButtonSubmit().click()
            cy.url({timeout:10000}).should('contain', 'https://investree.tech/v3/borrower/data-completion');

       
    })
 it('Create Shareholder With Valid Credendtial Should Success',function(){
        Shareholder.getTabShareholder().click({force: true}) 
        Shareholder.getCreateInformationShareHolder().click({force: true})
        Shareholder.getShareholderPosition().click({force: true})
        Shareholder.getShareholderPositionOption1().click({force: true})
        Shareholder.getShareholderFullname().type(`Fullname${randomAlphabet()}`,{force: true})
        Shareholder.getShareholderKodeNegara().click({force: true})
        Shareholder.getShareholderKodeNegaraIndo().click({force: true})
        Shareholder.getShareholderHP().type(randomNumber(12),{force: true})
        Shareholder.getShareholderEmail().type(`test${randomNumber()}@gmail.com`,{force: true})
        Shareholder.getShareholderSaham().type(randomNumber(1),{force: true})
        Shareholder.getShareholderDate().click({force: true})
        Shareholder.getShareholderDate1().click({force: true})
        Shareholder.getShareholderMonth().click({force: true})
        Shareholder.getShareholderMonth1().click({force: true})
        Shareholder.getShareholderYear().click({force: true})
        Shareholder.getShareholderYear1993().click({force: true})
        const imageFile = 'a.jpg'
        cy.get('[data-unq="input-file-shareholders-id-card"]').attachFile(imageFile)
        cy.get('[data-unq="input-file-shareholders-selfie"]').attachFile(imageFile)
        cy.get('[data-unq="input-file-shareholders-tax-card"]').attachFile(imageFile)
       
       
        Shareholder.getShareholderKtp().type(randomNumber(16),{force:true})
        Shareholder.getShareholderKtpSeumurHidup().click({force:true})
        Shareholder.getShareholderNpwp().type(randomNumber(15),{force:true})
    
        Shareholder.getButtonSaveInformation().click()


       
         cy.url({timeout:10000}).should('contain', 'https://investree.tech/v3/borrower/data-completion');
    })
    it('Create Shareholder With invalid Credendtial Should Success',function(){
        Shareholder.getTabShareholder().click({force: true}) 
        Shareholder.getCreateInformationShareHolder().click({force: true})
        Shareholder.getShareholderPosition().click({force: true})
        Shareholder.getShareholderPositionOption1().click({force: true})
        Shareholder.getShareholderFullname().type(`Fullname${randomAlphabet()}`,{force: true})
        Shareholder.getShareholderKodeNegara().click({force: true})
        Shareholder.getShareholderKodeNegaraIndo().click({force: true})
        Shareholder.getShareholderHP().type(randomNumber(15),{force: true})
        Shareholder.getShareholderEmail().type(`test${randomNumber()}@gmai,l.com`,{force: true})
        Shareholder.getShareholderSaham().type(randomNumber(1),{force: true})
        Shareholder.getShareholderDate().click({force: true})
        Shareholder.getShareholderDate1().click({force: true})
        Shareholder.getShareholderMonth().click({force: true})
        Shareholder.getShareholderMonth1().click({force: true})
        Shareholder.getShareholderYear().click({force: true})
        Shareholder.getShareholderYear1993().click({force: true})
        const imageFile = 'a.jpg'
        cy.get('[data-unq="input-file-shareholders-id-card"]').attachFile(imageFile)
        cy.get('[data-unq="input-file-shareholders-selfie"]').attachFile(imageFile)
        cy.get('[data-unq="input-file-shareholders-tax-card"]').attachFile(imageFile)
       
       
        Shareholder.getShareholderKtp().type(randomNumber(16),{force:true})
        Shareholder.getShareholderKtpSeumurHidup().click({force:true})
        Shareholder.getShareholderNpwp().type(randomNumber(15),{force:true})
    
        Shareholder.getButtonSaveInformation().should('be.disabled');


       
  
    })
 

})

function randomNumber(length = 9) {
    const pool = '1234567890';

    let result = new Random().string(length, pool);
    if (result.charAt(0) === '0') {
        const randomNum = new Random().integer(1, 9);
        result = randomNum + result.slice(1, result.length);
    }

    return result;
}

function randomAlphabet(length = 12) {
    const pool = 'abcdefghijklmnopqrstuvwxyz';

    let result = new Random().string(length, pool);
    if (result.charAt(0) === '0') {
        const randomAlpha = new Random().string(1, 25);
        result = randomAlpha + result.slice(1, result.length);
    }

    return result;
}