export default class Shareholder{
    static getTabShareholder(){
        return cy.get('[data-unq="data-completion-advance-info-tab-5"]')
    }
    static getCreateInformationShareHolder(){
        return cy.get('[data-unq="btn-add-shareholders-information"]',{timeout:20000})
    }
    static getShareholderPosition(){
        return cy.get('[data-unq="shareholders-position"]')
        
    }   
    static getShareholderPositionOption1(){
        return cy.get('[data-unq="shareholders-position-options-0"]')
        
    }
    static getShareholderFullname(){
        return cy.get('[data-unq="shareholders-fullname"]')

    }
    static getShareholderKodeNegara(){
        return cy.get('[data-unq="shareholders-mobile-prefix"]')

    }
    static getShareholderKodeNegaraIndo(){
        return cy.get('[data-unq="shareholders-mobile-prefix-result-0"]')

    }
    static getShareholderHP(){
        return cy.get('[data-unq="shareholders-mobile-number"]')
        
    }
    static getShareholderEmail(){
        return cy.get('[data-unq="shareholders-email"]')

    }
    static getShareholderSaham(){
        return cy.get('[data-unq="shareholders-stock-ownership"]')
        
    }
    static getShareholderDate(){
        return cy.get('[data-unq="select-day-shareholders-dob"]')
        
    }
    static getShareholderDate1(){
        return cy.get('[data-unq="select-day-shareholders-dob-options-0"]')
        
    }
    static getShareholderMonth(){
        return cy.get('[data-unq="select-month-shareholders-dob"]')
        
    }
    static getShareholderMonth1(){
        return cy.get('[data-unq="select-month-shareholders-dob-options-0"]')
        
    }
    static getShareholderYear(){
        return cy.get('[data-unq="select-year-shareholders-dob"]')
        
    }
    static getShareholderYear1993(){
        return cy.get('[data-unq="select-year-shareholders-dob-options-32"]')
        
    }
    static getShareholderKtp(){
        return cy.get('[data-unq="shareholders-id-card-number"]')
        
    }
    static getShareholderKtpSeumurHidup(){
        return cy.get('[data-unq="shareholders-lifetime"]')
        
    }
    static getShareholderNpwp(){
        return cy.get('[data-unq="shareholders-tax-card-number"]')
        
    }
    static getButtonSaveInformation(){
        return cy.get('[data-unq="btn-save-legal-info"]')
        
    }
    
    static getButtoEdit(){
        return cy.get('[dataunq="btn-shareholders-information-edit-0"]')
        
    }
    static getButtonDelete(){

        return cy.get('[dataunq="btn-shareholders-information-delete-0"]')
    }
    static getbuttonokDelete(){
        return cy.get('[data-unq="btn-modal-ok"]')
        
    }
    static getButtonCloseOk(){
        return cy.get('[data-unq="btn-close-ok"]')
        
    }
    static getButtoCancel(){
        return cy.get('[data-unq="btn-modal-cancel"]')
         
    }
}
