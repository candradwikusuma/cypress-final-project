export default class Register{
    static getRegistrationButton(){
        return cy.get('[data-unq="registration-borrower"]')
    }
    static getRegistrationIndividual(){
        return cy.get('[data-unq="btn-beneficial-owner-0"]')
    }
    static getRegistrationSubmitBeneficalOwner(){
        return cy.get('[data-unq="btn-submit-beneficial-owner"]')
    }
    static getSalutation(){
        return cy.get('[data-unq="auth-registration-salutation"]')
    }
    static getSalutationOption(){
        return cy.get('[data-unq="auth-registration-salutation-options-0"]')
    }
    static getFullName(){
        return cy.get('[data-unq="auth-registration-fullname"]')
    }
    static getEmail(){
        return cy.get('[data-unq="auth-registration-email"]')
    }
    static getHp(){
        return cy.get('[data-unq="auth-registration-phone-number"]')
    }
    static getNamaPengguna(){
        return cy.get('[data-unq="auth-registration-username"]')
        
    }
    static getPassword(){
        return cy.get('[data-unq="auth-registration-password"]')
    }
    static getAgreePrivacy(){
        return cy.get('[data-unq="auth-registration-agree-privacy"]')
    }
    static getSubscribe(){
        return cy.get('[data-unq="auth-registration-agreeSubscribe"]')

    }
    static getSubmitButtonRegistration(){
        return cy.get('[btn-unq="auth-registration-btn-submit"]')
    }
    static getOTP1(){
        return cy.get('[data-unq="auth-otp-otp-component-0"')
    }
    static getOTP2(){
        return cy.get('[data-unq="auth-otp-otp-component-1"')
    }
    static getOTP3(){
        return cy.get('[data-unq="auth-otp-otp-component-2"')
    }
    static getOTP4(){
        return cy.get('[data-unq="auth-otp-otp-component-3"')
    }
    static getOTP5(){
        return cy.get('[data-unq="auth-otp-otp-component-4"')
    }
    static getOTP6(){
        return cy.get('[data-unq="auth-otp-otp-component-5"')
    }
    //preference
    static getBorrowerIntitusi(){
        return cy.get('[data-unq="btn-account-preference-1"]')
    }
    static getEditCompany(){
        return cy.get('[data-unq="select-edit-company-type"]')
    }
    static getSelectedCompany(){
        return cy.get('[data-unq="select-edit-company-type-options-0"]')
    }
    static getCompanyName(){
        return cy.get('[data-unq="preference-input-company-name"]')
    }
    static getSemuaProduk(){
        return cy.get('[data-unq="btn-product-preference-2"]')
    }
    static getSemuaProduk(){
        return cy.get('[data-unq="btn-product-preference-2"]')
    }
    static getProjectFinancing(){
        return cy.get('[data-unq="select-product-selection-pf"]')
    }
    static getSubmitContinue(){
        return cy.get('[data-unq="preference-submit-continue"]')
    }
    //login
    static getButtonSubmit(){
        return cy.get('[data-unq="btn-submit"]')
    }
    static getEmailLogin(){
        return cy.get('[data-unq="email"]')

    }
    static getPasswordLogin(){
        return cy.get('[data-unq="password"]')
        
    }
    static getErrorMessage(){
        return cy.get('[data-unq="error-msg"]',{timeout:10000})

    }
}